package serv1;
import com.panamahitek.ArduinoException;
import com.panamahitek.PanamaHitek_Arduino;
import com.panamahitek.PanamaHitek_MultiMessage;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import java.net.*; 
import java.io.*; 
import jssc.SerialPort;
import static serv1.iot.count;

public class iot {
    static  int count=0;
  static public void servidor(){
                    DatosEnviados = "#iott#1#"+count+"#";
            escritor.println (DatosEnviados);
             System.out.println("envio datos");
   }
  
  static PrintWriter escritor = null; 
  static String DatosEnviados = null; 
  public static void main(String[] argumentos) 
  throws IOException, InterruptedException, ArduinoException, SerialPortException{
  int n=0;
  Socket cliente = null; 
  BufferedReader entrada=null;
  String maquina; 
  int puerto; 
  BufferedReader DatosTeclado = new BufferedReader ( new InputStreamReader (System.in)); 
  if (argumentos.length != 2){ 
  maquina = "localhost"; 
  puerto = 12345; 
  System.out.println ("Establecidos valores por defecto:\nEQUIPO = localhost\nPORT = 12345"); 
  } 
  else{ 
  maquina = argumentos[0]; 
  Integer pasarela = new Integer (argumentos[1]); 
  puerto = pasarela.parseInt(pasarela.toString()); 
  System.out.println ("Conectado a " + maquina + " en puerto: " + puerto); 
  } 
  try{ 
  cliente = new Socket (maquina,puerto); 
  }catch (Exception e){ 
  System.out.println ("Fallo : "+ e.toString()); 
  System.exit (0); 
		}
try{ 
escritor = new PrintWriter(cliente.getOutputStream(), true);
entrada=new BufferedReader(new InputStreamReader(cliente.getInputStream()));
}catch (Exception e){ 
System.out.println ("Fallo : "+ e.toString()); 
cliente.close(); 
System.exit (0); 
} 
String line;	
System.out.println("Conectado con el Servidor. Listo para enviar datos...");
System.out.println("Envíe peticion de inicio de sesion:");
PanamaHitek_Arduino ino =new PanamaHitek_Arduino();
PanamaHitek_MultiMessage multi=new PanamaHitek_MultiMessage(3,ino );
SerialPortEventListener listener;
listener = new SerialPortEventListener(){
@Override
public void serialEvent(SerialPortEvent spe) {
System.out.println("serialevent");
try {
if(multi.dataReceptionCompleted()){
System.out.println("if");
System.out.println("\n");
System.out.println(multi.getMessage(0));
System.out.println(multi.getMessage(1));
System.out.println(multi.getMessage(2));
count++;
System.out.println(count);
servidor();
System.out.println("Envie als ervidor");
multi.flushBuffer();
}
} catch (ArduinoException ex) {
Logger.getLogger(iot.class.getName()).log(Level.SEVERE, null, ex);
} catch (SerialPortException ex) {
Logger.getLogger(iot.class.getName()).log(Level.SEVERE, null, ex);
}}};
System.out.println("Llamo arduino");
ino.arduinoRX("COM6", 115200, listener);
System.out.println("termina arduino");
do{ 
DatosEnviados = "#iott#1#"+count+"#";
while (!DatosEnviados.equals("FIN")); 
System.out.println ("Finalizada conexion con el servidor"); 
try{ 
escritor.close(); 
}catch (Exception e){}  

