/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author ednaedel
 */
public class servidorCliente {
   String maquina; 
    int puerto; 
    public servidorCliente(){}

    public servidorCliente(String maquina, int puerto) {
        this.maquina = maquina;
        this.puerto = puerto;
    }

    public String getMaquina() {
        return maquina;
    }

    public void setMaquina(String maquina) {
        this.maquina = maquina;
    }

    public int getPuerto() {
        return puerto;
    }

    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }
    
    
    public String Peticion(String a)throws IOException{
        Socket cliente = null; 
		PrintWriter escritor1 = null; 
		String DatosEnviados = null; 
		BufferedReader entrada=null;
                    maquina = this.getMaquina();
			Integer pasarela = new Integer (this.getPuerto()); 
			puerto = pasarela.parseInt(pasarela.toString()); 
			System.out.println ("Conectado a " + maquina + " en puerto: " + puerto); 
                        try{ 
			cliente = new Socket (maquina,puerto); 
		}catch (Exception e){ 
			System.out.println ("Fallo : "+ e.toString()); 
			System.exit (0); 
		}
		try{ 
			escritor1 = new PrintWriter(cliente.getOutputStream(), true);
			entrada=new BufferedReader(new InputStreamReader(cliente.getInputStream()));
 
		}catch (Exception e){ 
			System.out.println ("Fallo : "+ e.toString()); 
			cliente.close(); 
			System.exit (0); 
		} 
		String  line;
		
		System.out.println("Conectado con el Servidor. Listo para enviar datos...");
                
              
			DatosEnviados = a; 
			escritor1.println (DatosEnviados); 
			line = entrada.readLine();
                     
			System.out.println(line);
                        
	
		System.out.println ("Finalizada conexion con el servidor"); 
		try{ 
			escritor1.close(); 
		}catch (Exception e){}
                return line;
    }  
}
