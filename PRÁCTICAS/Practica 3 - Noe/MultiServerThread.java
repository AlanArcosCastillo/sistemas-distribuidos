import java.net.*;
import java.io.*;
import java.util.Date;

public class MultiServerThread extends Thread {
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
		
	     while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();
            if(lineIn.equals("FIN")){
               ServerMultiClient.NoClients--;
			      break;
			   }else{
                 
               //VERIFICAR CLIENTES CONECTADOS AL SERVIDOR 
                if(lineIn.equals("#cuantos#"))
                {
                 escritor.println(ServerMultiClient.NoClients);
                 escritor.flush();
                }
                
                else
                {
                 escritor.println("Echo... "+lineIn);
                 escritor.flush();
                }
                
                         
                
                 
                
                
                
                
                
                
                
                
                
                //4 MINUSCULAS
                //PETICION: #min#1#cadena"
                //RESPUESTA #r: min#1#cadena#
                
                //5 CONVERTIR UN NUMERO A SU TEXTO
                //PETICION: #num2text#1#137"
                //RESPUESTA #r: num2text#1#Ciento treinta y siete#
                
                //6 DECIMAL A BINARIO
                //PETICION: #dec2bin#1#NumeroDecimal"
                //RESPUESTA #r: dec2bin#1#NumeroBinario#
                
                 
            }
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
} 
