import java.net.*; 
import java.io.*; 
import java.util.Date;
import java.util.Scanner;
public class ClientFullDuplex { 
	public static void main (String[] argumentos)throws IOException{ 
		Socket cliente = null; 
		PrintWriter escritor = null; 
		String DatosEnviados = null; 
                String entra="";
                String salida="";
                String cadenafinal;
                String cadena1;
                String cadena2;
                String cadena3;
                BufferedReader entrada=null;
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int opc=0;
                
           
              
		String maquina; 
		int puerto; 
		BufferedReader DatosTeclado = new BufferedReader ( new InputStreamReader (System.in)); 

		if (argumentos.length != 2){ 
			maquina = "localhost"; 
			puerto = 12345; 
			System.out.println ("Establecidos valores por defecto:\nEQUIPO = localhost\nPORT = 12345"); 
		} 
		else{ 
			maquina = argumentos[0]; 
			Integer pasarela = new Integer (argumentos[1]); 
			puerto = pasarela.parseInt(pasarela.toString()); 
			System.out.println ("Conectado a " + maquina + " en puerto: " + puerto); 
		} 
		try{ 
			cliente = new Socket (maquina,puerto); 
		}catch (Exception e){ 
			System.out.println ("Fallo : "+ e.toString()); 
			System.exit (0); 
		}
		try{ 
			escritor = new PrintWriter(cliente.getOutputStream(), true);
			entrada=new BufferedReader(new InputStreamReader(cliente.getInputStream()));
 
		}catch (Exception e){ 
			System.out.println ("Fallo : "+ e.toString()); 
			cliente.close(); 
			System.exit (0); 
		} 
		String line;
		
		System.out.println("Conectado con el Servidor. Listo para enviar datos..\n");
               
		do{ 
                    System.out.println("Escoge una opción: ");
                    System.out.println("1.- VER CANTIDAD CLIENTES CONECTADOS AL SERVIDOR");
                    System.out.println("2.- INVERTIR CADENA");
                    System.out.println("3.- CONCATENAR CADENAS");
                    System.out.println("4.- CONVERTIR A MAYUSCULAS");
                    System.out.println("5.- CONVERTIR A MINUSCULAS");
                    System.out.println("6.- CONVERTIR UN NUMERO A SU TEXTO");
                    System.out.println("7.- CONVERTIR UN NUMERO DE DECIMAL A BINARIO");
                    
                    Scanner reader = new Scanner(System.in);
                    opc = reader.nextInt();
                   
                    switch (opc){
                    case 1:
                    {
                        System.out.println("\nEnvía cadena #cuantos#\n");
                        DatosEnviados = DatosTeclado.readLine();
		        escritor.println (DatosEnviados); 
		        line = entrada.readLine();
		        System.out.println("Hay "+line+" Usuarios conectados en el servidor\n");
                        break;
                    }
                    
                    
                      //2 INVERTIR CADENA
                      //Petición: #inv#1#cadena#
                      //Respuesta: #r.inv#1#cadena-invertida#
                    
                    case 2:
                    {
                        System.out.println("Ingresa cadena a invertir");
                        System.out.print("#inv#1");
                        entra=br.readLine();
                        for(int i=entra.length()-1;i>=0;i--){
                        salida= salida + entra.charAt(i);
                        }
                        System.out.println("r:inv#1"+salida);
                        break;
                    }
                    
                    //3 CONCATENAR CADENAS
                    //PETICION: #concat#n#cad1#...#cadn"
                    //RESPUESTA #r: concat#1#cad1cad2...cadn#
                    //EJEMPLO: P: #concat#3#hoy#si#trabaje#
                    //R: #r:concat#1#HoySíTrabajé#
                           
                     case 3:
                    {
                       System.out.println("Ingresa cadena 1");
                       cadena1=br.readLine();
                       System.out.print("Ingresa cadena 2");
                       cadena2=br.readLine();
                       System.out.print("Ingresa cadena 3");
                       cadena3=br.readLine();  
                       cadenafinal = cadena1+cadena2+cadena3;
                       System.out.println("#r:concat#3"+cadenafinal+"#");  
                       break;
                    }
                    
                    
                    
                    //4 CONVERTIR A MAYUSCULAS
                    //PETICION: #may#1#cadena"
                    //RESPUESTA #r: may#1#cadena#
                    
                     case 4:
                    {
                        String miCadena;
                        System.out.println("Ingrese cadena:");
                        miCadena=br.readLine();
                        System.out.println(miCadena.toUpperCase());
                        break;
                    }
                    
                    //5 CONVERTIR A MINUSCULAS
                    //PETICION: #min#1#cadena"
                    //RESPUESTA #r: min#1#cadena#
                    
                     case 5:
                    {
                        String miCadena;
                        System.out.println("Ingrese cadena:");
                        miCadena=br.readLine();
                        System.out.println(miCadena.toLowerCase());
                        break;
                    }
                    
                     //5 CONVERTIR UN NUMERO A SU TEXTO
                     //PETICION: #num2text#1#137"
                     //RESPUESTA #r: num2text#1#Ciento treinta y siete#
                     case 6:
                    {
                        
                        break;
                    }
                    
                    case 7:
                    {
                        System.out.println("Escogiste opción 6");
                        break;
                    }
                    
                     default:
                     {
                        System.out.println("Opción inválida man :/"); 
                        break;
                     }
                    
                    
                    }
                 
			
		}while (!DatosEnviados.equals("FIN")); 
	
		System.out.println ("Finalizada conexion con el servidor"); 
		try{ 
			escritor.close(); 
		}catch (Exception e){}
	}
} 

